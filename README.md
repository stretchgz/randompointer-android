# README #

Tap a button to generate a random pointing arrow in an Android app.

### What is this repository for? ###

* Android beginner
* Version: 0.0.1

### How do I get set up? ###

1. Get Android Studio 2.0 (https://developer.android.com/sdk/index.html)
2. Download source code, either
    * using command-line `git clone https://bitbucket.org/stretchgz/pro336.git` 
    * or go to Download and click "Download repository" (https://bitbucket.org/stretchgz/pro336/downloads)
3. Open Android Studio 2, click "Open an existing Android Studio project", locate and open the project directory
4. Start hacking

### License ###
* [MIT](https://opensource.org/licenses/MIT)