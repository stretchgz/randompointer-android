package im.wdt.apps.randomarrow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // const
        final Button btn = (Button) findViewById(R.id.button);
        final ImageView arrowLeft = (ImageView) findViewById(R.id.imageView);
        final ImageView arrowRight = (ImageView) findViewById(R.id.imageView2);
        // reset alpha and visibility
        arrowLeft.setAlpha(0f);
        arrowLeft.setVisibility(View.VISIBLE);
        arrowRight.setAlpha(0f);
        arrowRight.setVisibility(View.VISIBLE);

        if (btn != null) {
            // on tap, generates a random arrow (left/right)
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Random r = new Random();
                    Integer num = r.nextInt(2);
                    // hide tap button
                    btn.setVisibility(View.GONE);
                    // show left or right imageview
                    if (num == 1) {
                        arrowLeft.animate().alpha(1f).setDuration(2000)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        btn.setVisibility(View.VISIBLE);
                                        arrowLeft.setAlpha(0f);
                                    }
                                });
                    } else {
                        arrowRight.animate().alpha(1f).setDuration(2000)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        btn.setVisibility(View.VISIBLE);
                                        arrowRight.setAlpha(0f);
                                    }
                                });
                    }
                }
            });
        }
    }
}
